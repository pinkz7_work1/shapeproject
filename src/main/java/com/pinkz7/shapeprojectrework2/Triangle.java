/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeprojectrework2;

/**
 *
 * @author ripgg
 */
public class Triangle {
    double l;
    double h;
    public Triangle(double l, double h){
        this.l=l;
        this.h=h;
    }
    public double calArea(){
        return (l*h)/2;
    }
    public double getL(){
        return l;
    }
    public double getH(){
        return h;
    }
    public void setR(double l, double h){
        if((l<=0||h<=0)){
            System.out.println("Error: long and high must more than zero");
            return;
        }
        this.l=l;
        this.h=h;
    } 
    @Override
    public String toString(){
        return "Area of triangle1(r = "+this.getL()+" * "+ this.getH()+") is "+this.calArea();
    }
}
