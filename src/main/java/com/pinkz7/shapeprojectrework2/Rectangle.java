/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeprojectrework2;

/**
 *
 * @author ripgg
 */
public class Rectangle {
    double w;
    double l;
    public static final double pi=22.0/7;
    public Rectangle(double w, double l){
        this.w=w;
        this.l=l;
    }
    public double calArea(){
        return w*l;
    }
    public double getW(){
        return w;
    }
    public double getL(){
        return l;
    }
    public void setR(double w, double l){
        if((w<=0||l<=0)&&l>w){
            System.out.println("Error: wide and long must more than zero");
            return;
        }
        this.w=w;
        this.l=l;
    }
    @Override
    public String toString(){
        return "Area of rectangle1(r = "+this.getW()+" * "+ this.getL()+") is "+this.calArea();
    }
}
