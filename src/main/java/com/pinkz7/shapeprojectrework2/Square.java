/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeprojectrework2;

/**
 *
 * @author ripgg
 */
public class Square {
    double s;
    public Square(double s){
        this.s=s;
    }
    public double calArea(){
        return s*s;
    }
    public double getR(){
        return s;
    }
    public void setR(double s){
        if(s<=0){
            System.out.println("Error: Side must more than zero");
            return;
        }
        this.s=s;
    }
    public String toString(){
        return "Area of square1(r = "+this.getR()+") is "+this.calArea();
    }
}
