/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeprojectrework2;

/**
 *
 * @author ripgg
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(7,11);
        System.out.println("Area of rectangle1(w = "+rectangle1.getW()+" * l = "+ rectangle1.getL()+ ") is "+rectangle1.calArea()); 
        rectangle1.setR(5,13);
        System.out.println("Area of rectangle1(w = "+rectangle1.getW()+" * l = "+ rectangle1.getL()+ ") is "+rectangle1.calArea());
        rectangle1.setR(0,20);
        System.out.println("Area of rectangle1(w = "+rectangle1.getW()+" * l = "+ rectangle1.getL()+ ") is "+rectangle1.calArea()); 
        System.out.println(rectangle1.toString());
        System.out.println(rectangle1);
    }
}
